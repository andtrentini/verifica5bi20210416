# Verifica 5Bi 16 aprile 2021

### Materia: TPSIT
### Argomento: Docker

## Diagramma
![Diagramma soluzione](./Verifica%205Bi%2016%20aprile%202021.png) 

## Istruzioni
1. Creare su gitlab.com un progetto nel quale salvare il lavoro svolto con nome *NomeCognomeDocker20210416*, aggiungere **andtrentini** come **mantainer** del progetto.

2. Clonare il presente progetto sul proprio computer, rimuovere l'origine remota corrente ed aggiungere
il progetto creato precedentemente come origine remota.

3. Eseguire commit e push regolarmente (max ogni 10 minuti).

4. Consegnare il link del progetto sul compito classroom. 

### Struttura delle directory

    - Verifica5Bi20210416  
      |  
      +- car-angular-app: applicazione angular da siluppare per lo sviluppo  
      |  
      +- car-sql-server:  file sql per la creazione del database  
      |  
      +- car-web-server:  applicazione angular compilata pronta per essere ospitata su un server web  
      |  
      +- car-ws-server:   web service realizzato con Node + Express  
  
### Punto1: Realizzare i container per i servizi web server, ws server e sql server
  
#### web server
> Immagine da utilizzare: **nginx**  
> Nome del container: **car-web-server**  
> I file dell'applicazione da pubblicare su web si trovano nella directory **car-web-server**.  
> Il sito dovrà essere disponibile all'indirizzo http://localhost:8081  
  
#### ws server
> Immagine da utilizzare: **alpine**  
> Nome del container: **car-ws-server**  
> Installare nel container **node e npm**.  
> I file javascript del web service rest si trovano nella directory **car-ws-server**.  
> Dopo aver copiato i fle nel container inserire in Dockerfile le istruzioni per installare i packages utilizzati dal web service.  
> Il container deve essere connesso ad una rete interna con il container sql server ed accessibile all'indirizzo http://localhost:3000.  
> E' necessario fornire al web service la password dell'utente root (root) attraverso docker secret.  
> Mandare in esecuzione il servizio all'avvio del container (*node index.js*).
> Aggiungere un terminale al container.
  
#### sql server
> Immagine da utilizzare: **mysql:latest**  
> Nome del container: **car-sql-server**  
> I file per la creazione del database si trovano nella directory **car-sql-server**.  
> Il container deve essere connesso ad una rete interna con il container ws server e non dovrà essere accessibile da localhost.  
> Impostare la password dell'utente *root* (uguale a **root**) attraverso docker secret.
>
>> Utilizzare le variabili d'ambiente per impostare
>> - root password file
>> - database name: **automobili**
>> - user: **root**
  
### Punto 2 (opzionale) Creare un container in grado di ospitare lo sviluppo dell'applicazione angular.
  
### angular app
> Immagine da utilizzare: **alpine**  
> Nome del container: **car-angular-app**  
> Installare nel container **git, node e npm**.  
> Installare in global mode **angular**.  
> I file dell'applicazione sono contenuti nella directory **car-angular-app**.  
> Dopo aver copiato i fle nel container inserire in Dockerfile le istruzioni per installare i packages utilizzati dall'applicazione.  
> Aggiungere un terminale al container.  
> *Utilizzare Visual Studio Code per avviare l'applicazione*
