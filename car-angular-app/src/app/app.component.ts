import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  data: any;
  displayedColumns: string[] = ['id', 'Produttore', 'Modello', 'Anno'];
  
  constructor(private httpClient: HttpClient) {
  }

  public loadData() {
    this.httpClient.get('http://localhost:3000/').subscribe((data: any) => {
      this.data = data;      
    })
  }
}
