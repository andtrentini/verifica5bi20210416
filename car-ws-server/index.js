const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const fs = require('fs');

const service = express();

service.use(cors());

var config = {
  localPort: 3000,
  dbParams: {
    host: 'car-sql-server',
    user: 'root',
    password: '',
    database: 'automobili'
  }
}

fs.readFile('/run/secrets/dbpassword', 'utf8', (err, data) => {
  if (err) {
    console.log(err)
  }
  config.dbParams.password = data;
})

service.get('/', (req, res) => {
  let connection = mysql.createConnection(config.dbParams);
  connection.query('SELECT * FROM automobili', (error, data) => {
    connection.end();
    if (!error) {
      res.json(data);
    } else {
      res.status(500).send(error);
    }
  })
});

var server = service.listen(config.localPort, () => {
  console.log("Server attivato...");
});